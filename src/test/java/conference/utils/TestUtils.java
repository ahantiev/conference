package conference.utils;

import org.springframework.test.web.servlet.ResultMatcher;
import conference.rest.error.ApiError;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public final class TestUtils {

    public static ResultMatcher isError(final ApiError apiError) {
        return result -> {
            status().is(apiError.getStatus().value()).match(result);
            jsonPath("$.code").value(apiError.getCode()).match(result);
            jsonPath("$.status").value(apiError.getStatus().value()).match(result);
        };
    }
}
