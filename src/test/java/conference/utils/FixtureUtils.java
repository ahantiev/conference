package conference.utils;

import conference.model.Conference;
import conference.model.Speech;
import conference.model.User;

public final class FixtureUtils {
    public static Conference createConferenceFixture() {
        return Conference.builder()
                .name("Conference name")
                .logoUrl("url")
                .description("description")
                .location("loc")
                .latitude(45.643478)
                .longitude(35.668568)
                .start(TimeUtils.now())
                .end(TimeUtils.now().plusDays(2))
                .newsRssUrl("rssUrl")
                .twitterFeedUrl("twitterFeedUrl")
                .faqUrl("faqUrl")
                .privacyPolicyUrl("privacyPolicyUrl")
                .licenseAgreementUrl("licenseAgreementUrl")
                .build();
    }

    public static Speech createSpeechFixture() {
        return Speech.builder()
                .location("loc")
                .end(TimeUtils.now())
                .start(TimeUtils.now().plusHours(1))
                .description("description")
                .build();
    }

    public static User createUserFixture() {
        return User.builder()
                .name("User")
                .build();
    }
}
