package conference.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsServiceTest {
    @Autowired
    private SmsService smsService;

    @Test
    public void testSending() throws Exception {
        smsService.send("79631021674", "test sms");
    }
}
