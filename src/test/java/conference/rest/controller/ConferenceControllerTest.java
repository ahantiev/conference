package conference.rest.controller;

import conference.Constants;
import conference.model.Conference;
import conference.model.Participation;
import conference.model.Speech;
import conference.model.Sponsor;
import conference.model.User;
import conference.repository.ConferenceRepository;
import conference.repository.ParticipationRepository;
import conference.repository.SpeechRepository;
import conference.repository.SponsorRepository;
import conference.repository.UserRepository;
import conference.utils.TestUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import conference.rest.error.ApiError;
import conference.security.token.UserTokenProvider;
import conference.utils.FixtureUtils;
import conference.utils.TimeUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConferenceControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SpeechRepository speechRepository;
    @Autowired
    private SponsorRepository sponsorRepository;
    @Autowired
    private ConferenceRepository conferenceRepository;
    @Autowired
    private ParticipationRepository participationRepository;
    @Autowired
    private UserTokenProvider userTokenProvider;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        participationRepository.deleteAll();
        speechRepository.deleteAll();
        sponsorRepository.deleteAll();
        conferenceRepository.deleteAll();
    }

    @After
    public void tearDown() throws Exception {
        TimeUtils.useSystemDefaultZoneClock();
    }

    @Test
    public void getConferences() throws Exception {
        Conference conference = FixtureUtils.createConferenceFixture();
        conference.setName("Bconf");
        conferenceRepository.save(conference);
        Conference conference2 = FixtureUtils.createConferenceFixture();
        conference2.setName("Aconf");
        conferenceRepository.save(conference2);
        Conference conference3 = FixtureUtils.createConferenceFixture();
        conference3.setName("C conf");
        conferenceRepository.save(conference3);

        mockMvc.perform(get("/conferences"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.[0].id").value(conference2.getId()))
                .andExpect(jsonPath("$.[1].id").value(conference.getId()))
                .andExpect(jsonPath("$.[2].id").value(conference3.getId()))
        ;
    }

    @Test
    public void getConference() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));

        Conference conference = FixtureUtils.createConferenceFixture();
        conference.setName("Bconf");
        conferenceRepository.save(conference);

        mockMvc.perform(get("/conferences/" + conference.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(conference.getId()))
                .andExpect(jsonPath("$.name").value(conference.getName()))
                .andExpect(jsonPath("$.logoUrl").value(conference.getLogoUrl()))
                .andExpect(jsonPath("$.description").value(conference.getDescription()))
                .andExpect(jsonPath("$.location").value(conference.getLocation()))
                .andExpect(jsonPath("$.latitude").value(conference.getLatitude()))
                .andExpect(jsonPath("$.longitude").value(conference.getLongitude()))
                .andExpect(jsonPath("$.start").value(conference.getStart()
                        .format(DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT))))
                .andExpect(jsonPath("$.end").value(conference.getEnd()
                        .format(DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT))))
                .andExpect(jsonPath("$.newsRssUrl").value(conference.getNewsRssUrl()))
                .andExpect(jsonPath("$.twitterFeedUrl").value(conference.getTwitterFeedUrl()))
                .andExpect(jsonPath("$.faqUrl").value(conference.getFaqUrl()))
                .andExpect(jsonPath("$.privacyPolicyUrl").value(conference.getPrivacyPolicyUrl()))
                .andExpect(jsonPath("$.licenseAgreementUrl").value(conference.getLicenseAgreementUrl()))
        ;
    }

    @Test
    public void getConferenceNotFound() throws Exception {
        mockMvc.perform(get("/conferences/543646"))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.CONFERENCE_NOT_FOUND))
        ;
    }

    @Test
    public void getSpeakers() throws Exception {
        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        User user = FixtureUtils.createUserFixture();
        user.setName("Buser");
        userRepository.save(user);

        User user2 = FixtureUtils.createUserFixture();
        user2.setName("Auser");
        userRepository.save(user2);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        Speech speech2 = FixtureUtils.createSpeechFixture();
        speech2.setConference(conference);
        speechRepository.save(speech2);

        Participation participation = new Participation(conference, user, speech);
        participationRepository.save(participation);

        Participation participation1 = new Participation(conference, user2, speech2);
        participationRepository.save(participation1);

        mockMvc.perform(get("/conferences/" + conference.getId() + "/speakers"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(user2.getId()))
                .andExpect(jsonPath("$.[1].id").value(user.getId()))

        ;
    }

    @Test
    public void getGuests() throws Exception {
        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        User user = FixtureUtils.createUserFixture();
        user.setName("Buser");
        userRepository.save(user);

        User user2 = FixtureUtils.createUserFixture();
        user2.setName("Auser");
        userRepository.save(user2);

        Participation participation = new Participation(conference, user);
        participationRepository.save(participation);

        Participation participation1 = new Participation(conference, user2);
        participationRepository.save(participation1);

        mockMvc.perform(get("/conferences/" + conference.getId() + "/guests"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(user2.getId()))
                .andExpect(jsonPath("$.[1].id").value(user.getId()))

        ;
    }

    @Test
    public void getSponsors() throws Exception {
        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Sponsor sponsor = Sponsor.builder().name("Bsponsor").conference(conference).build();
        Sponsor sponsor2 = Sponsor.builder().name("Asponsor").conference(conference).build();
        sponsorRepository.save(Arrays.asList(sponsor, sponsor2));

        mockMvc.perform(get("/conferences/" + conference.getId() + "/sponsors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(sponsor2.getId()))
                .andExpect(jsonPath("$.[1].id").value(sponsor.getId()))

        ;
    }

}
