package conference.rest.controller;

import conference.model.Conference;
import conference.model.Participation;
import conference.model.Speech;
import conference.model.SpeechRating;
import conference.model.User;
import conference.repository.ConferenceRepository;
import conference.repository.ParticipationRepository;
import conference.repository.SpeechRatingRepository;
import conference.repository.SpeechRepository;
import conference.repository.UserRepository;
import conference.rest.dto.SpeechRatingDto;
import conference.rest.error.ApiError;
import conference.utils.JsonUtils;
import conference.utils.TestUtils;
import conference.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import conference.security.token.UserTokenProvider;
import conference.utils.FixtureUtils;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpeechControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private SpeechRepository speechRepository;
    @Autowired
    private SpeechRatingRepository speechRatingRepository;
    @Autowired
    private ConferenceRepository conferenceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ParticipationRepository participationRepository;
    @Autowired
    private UserTokenProvider userTokenProvider;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        TimeUtils.useSystemDefaultZoneClock();
        participationRepository.deleteAll();
        speechRatingRepository.deleteAll();
        speechRepository.deleteAll();
        conferenceRepository.deleteAll();
    }

    @Test
    public void getSpeeches() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));

        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speech.setStart(TimeUtils.now());
        speechRepository.save(speech);

        User user2 = FixtureUtils.createUserFixture();
        userRepository.save(user2);

        Conference conference2 = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference2);

        Speech speech2 = FixtureUtils.createSpeechFixture();
        speech2.setConference(conference2);
        speech2.setStart(TimeUtils.now().minusMinutes(1));
        speechRepository.save(speech2);

        Participation participation = new Participation(conference, user, speech);
        participationRepository.save(participation);

        Participation participation2 = new Participation(conference2, user2, speech2);
        participationRepository.save(participation2);

        mockMvc.perform(get("/speeches"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(speech2.getId()))
                .andExpect(jsonPath("$.[1].id").value(speech.getId()))

        ;
    }

    @Test
    public void getSpeechAvgRating() throws Exception {
        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        User user2 = FixtureUtils.createUserFixture();
        userRepository.save(user2);

        Speech speech2 = FixtureUtils.createSpeechFixture();
        speech2.setConference(conference);
        speechRepository.save(speech2);

        speechRatingRepository.save(Arrays.asList(
                SpeechRating.builder(speech, user).rating(1).build(),
                SpeechRating.builder(speech, user2).rating(5).build())
        );

        mockMvc.perform(get("/speeches/" + speech.getId() + "/avg-rating"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating").value(3))
        ;

        speechRatingRepository.deleteAll();
        speechRatingRepository.save(Arrays.asList(
                SpeechRating.builder(speech, user).rating(1).build(),
                SpeechRating.builder(speech, user2).rating(2).build())
        );

        mockMvc.perform(get("/speeches/" + speech.getId() + "/avg-rating"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating").value(1))
        ;
    }

    @Test
    public void setSpeechRating() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        mockMvc.perform(post("/speeches/" + speech.getId() + "/rating")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new SpeechRatingDto(5))))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        assertEquals(speechRatingRepository.count(), 1);

        mockMvc.perform(post("/speeches/" + speech.getId() + "/rating")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new SpeechRatingDto(5))))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        assertEquals(speechRatingRepository.count(), 1);
    }

    @Test
    public void setSpeechRatingInvalidValue() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        mockMvc.perform(post("/speeches/14646464/rating")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new SpeechRatingDto(6))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.VALIDATION))
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
        ;
    }

    @Test
    public void setSpeechRatingNotFoundSpeech() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        mockMvc.perform(post("/speeches/14646464/rating")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new SpeechRatingDto(5))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.SPEECH_NOT_FOUND))
        ;
    }
}
