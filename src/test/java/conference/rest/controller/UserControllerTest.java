package conference.rest.controller;

import conference.model.Registration;
import conference.model.User;
import conference.repository.RegistrationRepository;
import conference.repository.UserRepository;
import conference.rest.dto.UserDto;
import conference.rest.error.ApiError;
import conference.security.token.UserTokenProvider;
import conference.utils.JsonUtils;
import conference.utils.RandomUtils;
import conference.utils.TestUtils;
import conference.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private UserTokenProvider userTokenProvider;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        TimeUtils.useSystemDefaultZoneClock();
        registrationRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void updateUser() throws Exception {
        final User user = userRepository.save(new User());

        Registration registration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .user(user)
                .build();
        registrationRepository.save(registration);

        final UserDto dto = UserDto.builder()
                .name("name")
                .phoneNumber("79121234567")
                .photoUrl(null)
                .description("description")
                .university("university")
                .company("company")
                .position("position")
                .build();

        mockMvc.perform(put("/users/me")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(dto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(dto.getName()))
                .andExpect(jsonPath("$.phoneNumber").value(dto.getPhoneNumber()))
                .andExpect(jsonPath("$.photoUrl").value(dto.getPhotoUrl()))
                .andExpect(jsonPath("$.description").value(dto.getDescription()))
                .andExpect(jsonPath("$.university").value(dto.getUniversity()))
                .andExpect(jsonPath("$.company").value(dto.getCompany()))
                .andExpect(jsonPath("$.position").value(dto.getPosition()))
        ;

        User updatedUser = userRepository.findOne(user.getId());
        assertEquals(updatedUser.getRegistration().getIsFirstLogin(), false);
    }

    @Test
    public void updateUserValidationError() throws Exception {
        final User user = userRepository.save(new User());

        String longString = RandomUtils.randomAlphaNumeric(101);

        final UserDto dto = UserDto.builder()
                .name(longString)
                .phoneNumber(RandomUtils.randomNumeric(21))
                .photoUrl(null)
                .description("description")
                .university("university")
                .company(longString)
                .position(longString)
                .build();

        mockMvc.perform(put("/users/me")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(dto)))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.VALIDATION))
                .andExpect(jsonPath("$.fieldErrors", hasSize(4)))
        ;
    }

    @Test
    public void getUser() throws Exception {
        final User user = User.builder()
                .name("name")
                .phoneNumber("79121234567")
                .photoUrl(null)
                .description("description")
                .university("university")
                .company("company")
                .position("position")
                .build();
        userRepository.save(user);

        final User user2 = userRepository.save(new User());

        mockMvc.perform(get("/users/" + user.getId())
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user2.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(user.getName()))
                .andExpect(jsonPath("$.phoneNumber").value(user.getPhoneNumber()))
                .andExpect(jsonPath("$.photoUrl").value(user.getPhotoUrl()))
                .andExpect(jsonPath("$.description").value(user.getDescription()))
                .andExpect(jsonPath("$.university").value(user.getUniversity()))
                .andExpect(jsonPath("$.company").value(user.getCompany()))
                .andExpect(jsonPath("$.position").value(user.getPosition()))
        ;
    }

    @Test
    public void getCurrentUser() throws Exception {
        final User user = User.builder()
                .name("name")
                .phoneNumber("79121234567")
                .photoUrl(null)
                .description("description")
                .university("university")
                .company("company")
                .position("position")
                .build();
        userRepository.save(user);

        mockMvc.perform(get("/users/me")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(user.getName()))
                .andExpect(jsonPath("$.phoneNumber").value(user.getPhoneNumber()))
                .andExpect(jsonPath("$.photoUrl").value(user.getPhotoUrl()))
                .andExpect(jsonPath("$.description").value(user.getDescription()))
                .andExpect(jsonPath("$.university").value(user.getUniversity()))
                .andExpect(jsonPath("$.company").value(user.getCompany()))
                .andExpect(jsonPath("$.position").value(user.getPosition()))
        ;
    }

    @Test
    public void getUserNotFound() throws Exception {
        User user = userRepository.save(new User());

        mockMvc.perform(get("/users/5346586")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.USER_NOT_FOUND))
        ;
    }
}
