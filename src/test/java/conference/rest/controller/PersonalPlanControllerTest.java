package conference.rest.controller;

import conference.model.Conference;
import conference.model.PersonalPlan;
import conference.model.Speech;
import conference.model.User;
import conference.repository.ConferenceRepository;
import conference.repository.PersonalPlanRepository;
import conference.repository.SpeechRatingRepository;
import conference.repository.SpeechRepository;
import conference.repository.UserRepository;
import conference.utils.TestUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import conference.rest.error.ApiError;
import conference.security.token.UserTokenProvider;
import conference.utils.FixtureUtils;
import conference.utils.TimeUtils;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonalPlanControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private SpeechRepository speechRepository;
    @Autowired
    private SpeechRatingRepository speechRatingRepository;
    @Autowired
    private ConferenceRepository conferenceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PersonalPlanRepository personalPlanRepository;
    @Autowired
    private UserTokenProvider userTokenProvider;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        TimeUtils.useSystemDefaultZoneClock();
        personalPlanRepository.deleteAll();
    }

    @Test
    public void getPersonalPlan() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));

        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speech.setStart(TimeUtils.now());
        speechRepository.save(speech);

        Speech speech2 = FixtureUtils.createSpeechFixture();
        speech2.setConference(conference);
        speech2.setStart(TimeUtils.now().minusMinutes(1));
        speechRepository.save(speech2);

        personalPlanRepository.save(new PersonalPlan(speech, user));
        personalPlanRepository.save(new PersonalPlan(speech2, user));

        mockMvc.perform(get("/personal-plan")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id").value(speech2.getId()))
                .andExpect(jsonPath("$.[1].id").value(speech.getId()))

        ;
    }

    @Test
    public void addSpeechToPersonalPlan() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        assertEquals(personalPlanRepository.count(), 0);

        mockMvc.perform(post("/personal-plan/" + speech.getId())
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        assertEquals(personalPlanRepository.count(), 1);
    }

    @Test
    public void addSpeechToPersonalPlanNotFound() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        mockMvc.perform(post("/personal-plan/435636")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.SPEECH_NOT_FOUND))
        ;
    }

    @Test
    public void removeSpeechFromPersonalPlan() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        personalPlanRepository.save(new PersonalPlan(speech, user));

        assertEquals(personalPlanRepository.count(), 1);

        mockMvc.perform(delete("/personal-plan/" + speech.getId())
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        assertEquals(personalPlanRepository.count(), 0);
    }

    @Test
    public void removeSpeechFromPersonalPlanNotFoundSpeechInPlan() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        assertEquals(personalPlanRepository.count(), 0);

        mockMvc.perform(delete("/personal-plan/" + speech.getId())
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.PERSONAL_PLAN_SPEECH_NOT_FOUND))
        ;
    }

    @Test
    public void removeSpeechFromPersonalPlanNotFound() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        mockMvc.perform(delete("/personal-plan/6447568")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.SPEECH_NOT_FOUND))
        ;
    }

    @Test
    public void clearPersonalPlan() throws Exception {
        User user = FixtureUtils.createUserFixture();
        userRepository.save(user);

        Conference conference = FixtureUtils.createConferenceFixture();
        conferenceRepository.save(conference);

        Speech speech = FixtureUtils.createSpeechFixture();
        speech.setConference(conference);
        speechRepository.save(speech);

        Speech speech2 = FixtureUtils.createSpeechFixture();
        speech2.setConference(conference);
        speechRepository.save(speech2);

        personalPlanRepository.save(new PersonalPlan(speech, user));
        personalPlanRepository.save(new PersonalPlan(speech2, user));

        assertEquals(personalPlanRepository.count(), 2);

        mockMvc.perform(delete("/personal-plan/clean")
                .header(UserTokenProvider.USER_AUTH_TOKEN_HEADER, userTokenProvider.create(user.getId())))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        assertEquals(personalPlanRepository.count(), 0);
    }
}
