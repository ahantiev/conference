package conference.rest.controller;

import conference.model.Registration;
import conference.model.User;
import conference.repository.UserRepository;
import conference.rest.dto.RegisterConfirmDto;
import conference.rest.error.ApiError;
import conference.utils.JsonUtils;
import conference.utils.TestUtils;
import conference.utils.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import conference.repository.RegistrationRepository;
import conference.rest.dto.RegisterDto;
import conference.service.RegistrationService;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegistrationControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        TimeUtils.useSystemDefaultZoneClock();
        registrationRepository.deleteAll();
    }

    @Test
    public void register() throws Exception {
        final RegisterDto dto = new RegisterDto("+7 912 123 4567");

        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(dto)))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        final Registration registration = registrationRepository.findOneByPhoneNumber("79121234567")
                .orElseThrow(() -> new RuntimeException("Registration not found"));
        assertEquals(registration.getIsFirstLogin(), true);
    }

    @Test
    public void registerFailedInvalidPhoneNumber() throws Exception {
        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterDto("+6586778i7 912 123 4567"))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.REGISTER_INCORRECT_PHONE_NUMBER))
        ;

        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterDto("+7 912 9969978978123 4567"))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.REGISTER_INCORRECT_PHONE_NUMBER))
        ;
    }

    @Test
    public void registerWithExistingRegistration() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));

        final Registration newRegistration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .build();
        registrationRepository.save(newRegistration);

        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 10));
        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterDto("+7 912 123 4567"))))
                .andDo(print())
                .andExpect(status().isOk())
        ;

        final Registration oldRegistration = registrationRepository.findOneByPhoneNumber("79121234567")
                .orElseThrow(() -> new RuntimeException("Registration not found"));

        assertEquals(oldRegistration.getPhoneNumber(), "79121234567");
        assertNotEquals(oldRegistration.getSmsCode(), "12345");

        assertEquals(oldRegistration.getSmsCodeExpirationDate(),
                LocalDateTime.of(2017, 3, 10, 0, 10 + RegistrationService.SMS_CODE_EXPIRATION_TIME_IN_MINUTES));
    }

    @Test
    public void registerConfirm() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));

        final Registration newRegistration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .build();
        registrationRepository.save(newRegistration);

        mockMvc.perform(post("/register-confirm")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterConfirmDto("+7 912 123 4567", "12345"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andExpect(jsonPath("$.isFirstLogin").value(true))
        ;
    }

    @Test
    public void registerConfirmWithExistingUser() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));
        final User user = User.builder().name("Name").build();
        userRepository.save(user);

        final Registration newRegistration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .user(user)
                .isFirstLogin(false)
                .build();
        registrationRepository.save(newRegistration);

        mockMvc.perform(post("/register-confirm")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterConfirmDto("+7 912 123 4567", "12345"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andExpect(jsonPath("$.isFirstLogin").value(false))
        ;
    }

    @Test
    public void registerConfirmIncorrectPhoneOrCode() throws Exception {
        final Registration newRegistration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .build();
        registrationRepository.save(newRegistration);

        mockMvc.perform(post("/register-confirm")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterConfirmDto("+7 912 356 3446", "12345"))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.REGISTER_INCORRECT_PHONE_NUMBER_OR_CODE))
        ;

        mockMvc.perform(post("/register-confirm")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterConfirmDto("+7 912 123 4567", "99999"))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.REGISTER_INCORRECT_PHONE_NUMBER_OR_CODE))
        ;
    }

    @Test
    public void registerConfirmIsExpired() throws Exception {
        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 0));
        final Registration newRegistration = Registration.builder()
                .phoneNumber("79121234567")
                .smsCode("12345")
                .smsCodeExpirationDate(TimeUtils.now())
                .build();
        registrationRepository.save(newRegistration);

        TimeUtils.useFixedClockAt(LocalDateTime.of(2017, 3, 10, 0, 1));
        mockMvc.perform(post("/register-confirm")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(JsonUtils.toJson(new RegisterConfirmDto("+7 912 123 4567", "12345"))))
                .andDo(print())
                .andExpect(TestUtils.isError(ApiError.REGISTER_IS_EXPIRED))
        ;
    }


}
