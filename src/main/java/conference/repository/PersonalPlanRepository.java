package conference.repository;

import conference.model.PersonalPlan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonalPlanRepository extends CrudRepository<PersonalPlan, Long> {

    @Query("select p.speech.id from PersonalPlan p where p.user.id = :userId order by p.speech.start asc ")
    List<Long> findAllSpeechesIdsByUserId(@Param("userId") Long userId);

    Optional<PersonalPlan> findOneBySpeechIdAndUserId(Long speechId, Long userId);

    void deleteByUserId(Long userId);
}
