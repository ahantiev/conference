package conference.repository;

import conference.model.Registration;
import conference.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {
    Optional<User> findOneById(Long id);
    Optional<Registration> findOneByPhoneNumber(String formattedPhoneNumber);
    Optional<Registration> findOneByPhoneNumberAndSmsCode(String formattedPhoneNumber, String smsCode);
}
