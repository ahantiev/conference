package conference.repository;

import conference.model.Speech;
import conference.model.Sponsor;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SponsorRepository extends CrudRepository<Sponsor, Long> {

    Optional<Speech> findOneById(Long id);

    List<Sponsor> findAllByConferenceId(Long conferenceId, Sort sort);
}
