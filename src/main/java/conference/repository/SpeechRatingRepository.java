package conference.repository;

import conference.model.SpeechRating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpeechRatingRepository extends CrudRepository<SpeechRating, Long> {
    Optional<SpeechRating> findOneById(Long id);

    @Query("select avg(sr.rating) from SpeechRating sr where sr.speech.id = :speechId")
    Integer findAvgRatingBySpeechId(@Param("speechId") Long speechId);

    Optional<SpeechRating> findOneBySpeechIdAndUserId(Long speechId, Long userId);
}
