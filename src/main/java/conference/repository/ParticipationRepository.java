package conference.repository;

import conference.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import conference.model.Participation;

import java.util.List;

@Repository
public interface ParticipationRepository extends CrudRepository<Participation, Long> {

    @Query("select p from Participation p order by p.speech.start asc ")
    List<Participation> findAllOrderBySpeech();

    @Query("select p.user from Participation p where p.conference.id = :conferenceId and p.role = :role order by p.user.name")
    List<User> findAllByConferenceIdAndRole(@Param("conferenceId") Long conferenceId, @Param("role") Participation.Role role);

}
