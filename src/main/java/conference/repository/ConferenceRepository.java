package conference.repository;

import conference.model.Conference;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConferenceRepository extends CrudRepository<Conference, Long> {
    Optional<Conference> findOneById(Long id);
    List<Conference> findAll(Sort sort);
}
