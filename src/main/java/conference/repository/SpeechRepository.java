package conference.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import conference.model.Speech;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SpeechRepository extends CrudRepository<Speech, Long> {

    Optional<Speech> findOneById(Long id);

    @Query("select s from Speech s where s.id in (:ids) order by s.start asc")
    List<Speech> findAllByIdsOrderByStart(@Param("ids") Collection<Long> ids);
}
