package conference.service;

import conference.model.Registration;
import conference.model.User;
import conference.rest.error.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import conference.repository.RegistrationRepository;
import conference.repository.UserRepository;
import conference.rest.dto.UserDto;
import conference.rest.error.ApiError;
import conference.rest.mapper.UserMapper;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private UserMapper userMapper;

    @Transactional
    public UserDto update(Long userId, UserDto dto) {
        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        user.setName(dto.getName());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setPhotoUrl(dto.getPhotoUrl());
        user.setDescription(dto.getDescription());
        user.setUniversity(dto.getUniversity());
        user.setCompany(dto.getCompany());
        user.setPosition(dto.getPosition());

        Registration registration = user.getRegistration();
        if(registration.getIsFirstLogin()) {
            registration.setIsFirstLogin(false);
            registrationRepository.save(registration);
        }

        return userMapper.toDto(userRepository.save(user));
    }

    @Transactional(readOnly = true)
    public UserDto findById(Long userId) {
        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        return userMapper.toDto(user);
    }
}
