package conference.service;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import conference.model.Registration;
import conference.model.User;
import conference.repository.UserRepository;
import conference.rest.dto.RegisterConfirmDto;
import conference.rest.dto.RegisterConfirmResDto;
import conference.rest.error.ApiError;
import conference.rest.error.ApiException;
import conference.utils.RandomUtils;
import conference.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import conference.repository.RegistrationRepository;
import conference.rest.dto.RegisterDto;
import conference.security.token.UserTokenProvider;

import java.util.Optional;

@Service
public class RegistrationService {

    public static final int SMS_CODE_REGISTRATION_LENGTH = 5;
    public static final int SMS_CODE_EXPIRATION_TIME_IN_MINUTES = 2;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private UserTokenProvider userTokenProvider;
    @Autowired
    private SmsService smsService;

    @Transactional
    public void register(RegisterDto dto) {
        String formattedPhoneNumber = validateAndFormatPhoneNumber(dto.getPhoneNumber());
        Optional<Registration> registrationOptional = registrationRepository.findOneByPhoneNumber(formattedPhoneNumber);

        Registration registration;
        if (registrationOptional.isPresent()) {
            registration = updateRegistration(registrationOptional.get());
        } else {
            registration = createRegistration(formattedPhoneNumber);
        }

        registrationRepository.save(registration);
        smsService.send(registration.getPhoneNumber(), registration.getSmsCode());
    }

    @Transactional
    public RegisterConfirmResDto registerConfirm(RegisterConfirmDto dto) {
        final String formattedPhoneNumber = validateAndFormatPhoneNumber(dto.getPhoneNumber());
        Registration registration = registrationRepository.findOneByPhoneNumberAndSmsCode(formattedPhoneNumber, dto.getSmsCode())
                .orElseThrow(() -> new ApiException(ApiError.REGISTER_INCORRECT_PHONE_NUMBER_OR_CODE));

        if (TimeUtils.isExpired(registration.getSmsCodeExpirationDate())) {
            throw new ApiException(ApiError.REGISTER_IS_EXPIRED);
        }

        if (registration.getUser() == null) {
            final User user = User.builder()
                    .phoneNumber(registration.getPhoneNumber())
                    .build();
            userRepository.save(user);

            registration.setUser(user);
            registrationRepository.save(registration);
        }

        final String token = userTokenProvider.create(registration.getUser().getId());

        return new RegisterConfirmResDto(token, registration.getIsFirstLogin());
    }

    private Registration createRegistration(String phoneNumber) {
        return Registration.builder()
                .phoneNumber(phoneNumber)
                .smsCode(RandomUtils.randomNumeric(SMS_CODE_REGISTRATION_LENGTH))
                .smsCodeExpirationDate(TimeUtils.now().plusMinutes(SMS_CODE_EXPIRATION_TIME_IN_MINUTES))
                .build();
    }

    private Registration updateRegistration(Registration registration) {
        registration.setSmsCode(RandomUtils.randomNumeric(SMS_CODE_REGISTRATION_LENGTH));
        registration.setSmsCodeExpirationDate(TimeUtils.now().plusMinutes(SMS_CODE_EXPIRATION_TIME_IN_MINUTES));
        return registration;
    }

    private String validateAndFormatPhoneNumber(String phone) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        String formattedPhoneNumber;
        Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneUtil.parse(phone, PhoneNumberUtil.REGION_CODE_FOR_NON_GEO_ENTITY);
        } catch (NumberParseException e) {
            throw new ApiException(ApiError.REGISTER_INCORRECT_PHONE_NUMBER);
        }

        formattedPhoneNumber = phoneNumber.getCountryCode() + String.valueOf(phoneNumber.getNationalNumber());

        if (!phoneUtil.isValidNumber(phoneNumber)) {
            throw new ApiException(ApiError.REGISTER_INCORRECT_PHONE_NUMBER);
        }

        return formattedPhoneNumber;
    }
}
