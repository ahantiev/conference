package conference.service;

import conference.model.PersonalPlan;
import conference.model.User;
import conference.repository.PersonalPlanRepository;
import conference.rest.dto.SpeechDto;
import conference.rest.error.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import conference.model.Speech;
import conference.repository.SpeechRepository;
import conference.repository.UserRepository;
import conference.rest.error.ApiError;
import conference.rest.mapper.SpeechMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonalPlanService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PersonalPlanRepository personalPlanRepository;
    @Autowired
    private SpeechRepository speechRepository;
    @Autowired
    private SpeechMapper speechMapper;

    @Transactional(readOnly = true)
    public List<SpeechDto> getPersonalPlan(Long userId) {
        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        List<Long> speechIds = personalPlanRepository.findAllSpeechesIdsByUserId(user.getId());

        return speechRepository.findAllByIdsOrderByStart(speechIds)
                .stream()
                .map(speechMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public void addSpeechToPersonalPlan(Long userId, Long speechId) {
        final Speech speech = speechRepository.findOneById(speechId)
                .orElseThrow(() -> new ApiException(ApiError.SPEECH_NOT_FOUND));

        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        Optional<PersonalPlan> personalPlanOpt = personalPlanRepository.findOneBySpeechIdAndUserId(speech.getId(), user.getId());
        if (personalPlanOpt.isPresent()) {
            return;
        }

        personalPlanRepository.save(new PersonalPlan(speech, user));
    }


    @Transactional
    public void removeSpeechFromPersonalPlan(Long userId, Long speechId) {
        final Speech speech = speechRepository.findOneById(speechId)
                .orElseThrow(() -> new ApiException(ApiError.SPEECH_NOT_FOUND));

        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        final PersonalPlan personalPlan = personalPlanRepository.findOneBySpeechIdAndUserId(speech.getId(), user.getId())
                .orElseThrow(() -> new ApiException(ApiError.PERSONAL_PLAN_SPEECH_NOT_FOUND));
        personalPlanRepository.delete(personalPlan);
    }


    @Transactional
    public void clearPersonalPlan(Long userId) {
        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        personalPlanRepository.deleteByUserId(user.getId());
    }
}
