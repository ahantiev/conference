package conference.service;

import conference.model.Participation;
import conference.model.User;
import conference.repository.ParticipationRepository;
import conference.repository.SponsorRepository;
import conference.rest.dto.ConferenceDto;
import conference.rest.dto.SponsorDto;
import conference.rest.dto.UserDto;
import conference.rest.error.ApiError;
import conference.rest.error.ApiException;
import conference.rest.mapper.ConferenceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import conference.model.Conference;
import conference.repository.ConferenceRepository;
import conference.rest.mapper.SponsorMapper;
import conference.rest.mapper.UserMapper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ConferenceService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SponsorMapper sponsorMapper;
    @Autowired
    private ConferenceMapper conferenceMapper;
    @Autowired
    private SponsorRepository sponsorRepository;
    @Autowired
    private ConferenceRepository conferenceRepository;
    @Autowired
    private ParticipationRepository participationRepository;

    @Transactional(readOnly = true)
    public List<ConferenceDto> findAll() {
        Sort sort = new Sort(Sort.Direction.ASC, "name");
        return conferenceRepository.findAll(sort).stream()
                .map(conferenceMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ConferenceDto findById(Long id) {
        final Conference conference = conferenceRepository.findOneById(id)
                .orElseThrow(() -> new ApiException(ApiError.CONFERENCE_NOT_FOUND));
        return conferenceMapper.toDto(conference);
    }

    @Transactional(readOnly = true)
    public List<UserDto> findSpeakers(Long conferenceId) {
        final Conference conference = conferenceRepository.findOneById(conferenceId)
                .orElseThrow(() -> new ApiException(ApiError.CONFERENCE_NOT_FOUND));

        Iterable<User> conferences = participationRepository
                .findAllByConferenceIdAndRole(conference.getId(), Participation.Role.Speaker);
        return StreamSupport.stream(conferences.spliterator(), false)
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<UserDto> findGuests(Long conferenceId) {
        final Conference conference = conferenceRepository.findOneById(conferenceId)
                .orElseThrow(() -> new ApiException(ApiError.CONFERENCE_NOT_FOUND));

        Iterable<User> conferences = participationRepository
                .findAllByConferenceIdAndRole(conference.getId(), Participation.Role.Guest);
        return StreamSupport.stream(conferences.spliterator(), false)
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<SponsorDto> getSponsors(Long conferenceId) {
        final Conference conference = conferenceRepository.findOneById(conferenceId)
                .orElseThrow(() -> new ApiException(ApiError.CONFERENCE_NOT_FOUND));

        Sort sort = new Sort(Sort.Direction.ASC, "name");
        return sponsorRepository.findAllByConferenceId(conference.getId(), sort)
                .stream()
                .map(sponsorMapper::toDto)
                .collect(Collectors.toList());
    }
}
