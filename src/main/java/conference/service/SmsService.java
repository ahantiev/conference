package conference.service;

import conference.sms.client.SmsClient;
import conference.sms.dto.IQSendResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SmsService {

    @Autowired
    private SmsClient<IQSendResponse> smsClient;

    public void send(String phone, String message) {
        IQSendResponse response = smsClient.send(phone, message);

        if (response.isError()) {
            log.error("Error SMS sending", response.getCause());
            return;
        }

        response.getMessages().forEach(m -> {
            if (m.isOk()) {
                log.error("Error SMS sending", m);
            }
        });
    }
}
