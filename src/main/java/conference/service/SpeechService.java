package conference.service;

import conference.model.Participation;
import conference.model.Speech;
import conference.model.SpeechRating;
import conference.model.User;
import conference.rest.dto.SpeechAvgRatingDto;
import conference.rest.dto.SpeechDto;
import conference.rest.error.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import conference.repository.ParticipationRepository;
import conference.repository.SpeechRatingRepository;
import conference.repository.SpeechRepository;
import conference.repository.UserRepository;
import conference.rest.dto.SpeechRatingDto;
import conference.rest.error.ApiError;
import conference.rest.mapper.SpeechMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SpeechService {
    @Autowired
    private SpeechMapper speechMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SpeechRepository speechRepository;
    @Autowired
    private SpeechRatingRepository speechRatingRepository;
    @Autowired
    private ParticipationRepository participationRepository;

    @Transactional(readOnly = true)
    public List<SpeechDto> findAll() {
        List<Participation> participations = participationRepository.findAllOrderBySpeech();
        return participations.stream()
                .map(speechMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public SpeechAvgRatingDto findAvgRating(Long speechId) {
        Integer avgRating = speechRatingRepository.findAvgRatingBySpeechId(speechId);
        return new SpeechAvgRatingDto(avgRating);
    }

    @Transactional
    public void setSpeechRating(Long speechId, Long userId, SpeechRatingDto ratingDto) {
        final Speech speech = speechRepository.findOneById(speechId)
                .orElseThrow(() -> new ApiException(ApiError.SPEECH_NOT_FOUND));

        final User user = userRepository.findOneById(userId)
                .orElseThrow(() -> new ApiException(ApiError.USER_NOT_FOUND));

        final SpeechRating speechRating = speechRatingRepository.findOneBySpeechIdAndUserId(speech.getId(), user.getId())
                .orElseGet(() -> new SpeechRating(speech, user));

        speechRating.setRating(ratingDto.getRating());
        speechRatingRepository.save(speechRating);
    }
}
