package conference.utils;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class TimeUtils {

    private static Clock clock = Clock.systemDefaultZone();
    private static ZoneId zoneId = ZoneId.systemDefault();

    public static LocalDateTime now() {
        return LocalDateTime.now(getClock());
    }

    public static void useFixedClockAt(LocalDateTime date) {
        clock = Clock.fixed(date.atZone(zoneId).toInstant(), zoneId);
    }

    public static void useSystemDefaultZoneClock() {
        clock = Clock.systemDefaultZone();
    }

    public static boolean isExpired(LocalDateTime date) {
        return TimeUtils.now().isAfter(date);
    }

    private static Clock getClock() {
        return clock;
    }
}