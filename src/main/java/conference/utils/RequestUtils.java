package conference.utils;


import conference.rest.error.ApiError;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestUtils {

    public static void writeApiErrorToResponse(ApiError apiError, ServletResponse response) throws IOException {
        int value = apiError.getStatus().value();
        ((HttpServletResponse) response).setStatus(value);
        response.setContentType("application/json");
        response.getWriter().write(JsonUtils.toJsonString(apiError.toMap()));
    }

}
