package conference;

import conference.model.User;
import conference.rest.error.ApiError;
import conference.rest.error.ApiException;
import conference.security.authentication.UserAuthenticationToken;
import conference.security.authentication.UserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;


public class AppContext {

    public static void setPrincipal(UserPrincipal principal) {
        getSecurityContext().setAuthentication(new UserAuthenticationToken(principal));
    }

    public static User getAuthenticatedUser() {
        final UserAuthenticationToken authentication = getUserAuthenticationToken();
        final User user = authentication.getPrincipal().getUser();

        if (user == null) {
            throw new ApiException(ApiError.UNAUTHORIZED);
        }

        return user;
    }

    private static UserAuthenticationToken getUserAuthenticationToken() {
        final Authentication authentication = getSecurityContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            throw new ApiException(ApiError.UNAUTHORIZED);
        }

        if (!(authentication instanceof UserAuthenticationToken)) {
            throw new ApiException(ApiError.UNAUTHORIZED);
        }
        return (UserAuthenticationToken) authentication;
    }

    private static SecurityContext getSecurityContext() {
        return SecurityContextHolder.getContext();
    }
}
