package conference.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import conference.security.token.UserTokenProvider;

@Configuration
public class JwtConfig {


    @Value("${auth.token.secret}")
    private String secret;

    @Value("${auth.token.expirationInMinutes}")
    private int expirationTimeInMinutes;

    @Bean
    public UserTokenProvider tokenProvider() {
        return new UserTokenProvider(secret, expirationTimeInMinutes);
    }
}
