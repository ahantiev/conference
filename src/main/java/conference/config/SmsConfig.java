package conference.config;

import conference.sms.client.SmsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import conference.AppEnvironment;
import conference.sms.client.FakeIQSmsClient;
import conference.sms.client.IQSmsClient;

@Configuration
public class SmsConfig {

    @Autowired
    private AppEnvironment env;
    @Value("${sms.iq.login}")
    private String smsClientLogin;
    @Value("${sms.iq.password}")
    private String smsClientPassword;

    @Bean
    public SmsClient smsClient() {
        if (env.isEmptyProfile()) {
            return new FakeIQSmsClient();
        }
        return new IQSmsClient(smsClientLogin, smsClientPassword);
    }
}
