package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Builder(builderMethodName = "privateBuilder")
@Table(name = "c_registration")
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(unique = true, name = "user_id")
    private User user;

    @NotBlank
    @Column(name = "phone_number", length = 20, unique = true, nullable = false)
    private String phoneNumber;

    @NotBlank
    @Column(name = "sms_code", length = 10, nullable = false)
    private String smsCode;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(name = "sms_code_expiration_date", nullable = false)
    private LocalDateTime smsCodeExpirationDate;

    @Column(name = "is_first_login", nullable = false)
    private Boolean isFirstLogin = true;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public Registration() {
    }

    public static RegistrationBuilder builder() {
        return privateBuilder().isFirstLogin(true);
    }
}
