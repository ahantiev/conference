package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Builder(builderMethodName = "privateBuilder")
@Table(name = "c_personal_plan")
public class PersonalPlan {
    @EmbeddedId
    private PersonalPlanId id;

    @ManyToOne
    @JoinColumn(name = "speech_id", insertable = false, updatable = false)
    private Speech speech;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public PersonalPlan() {
        this.id = new PersonalPlanId();
    }

    @Tolerate
    public PersonalPlan(Speech speech, User user) {
        this.speech = speech;
        this.user = user;
        this.id = new PersonalPlanId(speech.getId(), user.getId());
    }

    public static PersonalPlanBuilder builder(Speech speech, User user) {
        return privateBuilder()
                .id(new PersonalPlanId(speech.getId(), user.getId()))
                .speech(speech)
                .user(user);
    }
}