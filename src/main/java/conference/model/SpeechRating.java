package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Builder(builderMethodName = "privateBuilder")
@Table(name = "c_speech_rating")
public class SpeechRating {
    @EmbeddedId
    private SpeechRatingId id;

    @ManyToOne
    @JoinColumn(name = "speech_id", insertable = false, updatable = false)
    private Speech speech;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @Min(1)
    @Max(5)
    @Column(name = "rating")
    private Integer rating;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public SpeechRating() {
        this.id = new SpeechRatingId();
    }

    @Tolerate
    public SpeechRating(Speech speech, User user) {
        this.speech = speech;
        this.user = user;
        this.id = new SpeechRatingId(speech.getId(), user.getId());
    }

    public static SpeechRatingBuilder builder(Speech speech, User user) {
        return privateBuilder()
                .id(new SpeechRatingId(speech.getId(), user.getId()))
                .speech(speech)
                .user(user);
    }

}