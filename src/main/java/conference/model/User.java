package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Builder
@Table(name = "c_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Length(max = 20)
    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Lob
    @Column(name = "photo_url", columnDefinition="TEXT")
    private String photoUrl;

    @Lob
    @Column(name = "description", columnDefinition="TEXT")
    private String description;

    @Length(max = 100)
    @Column(name = "university", length = 100)
    private String university;

    @Length(max = 100)
    @Column(name = "company", length = 100)
    private String company;

    @Length(max = 100)
    @Column(name = "position", length = 100)
    private String position;

    @OneToOne(mappedBy = "user")
    private Registration registration;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public User() {
    }
}
