package conference.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Entity
@Table(name = "c_participation")
public class Participation {
    @EmbeddedId
    private ParticipationId id;

    @ManyToOne
    @JoinColumn(name = "conference_id", insertable = false, updatable = false)
    private Conference conference;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "speech_id")
    private Speech speech;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.Guest;

    public Participation() {
        this.id = new ParticipationId();
    }

    @Tolerate
    public Participation(@NonNull Conference conference, @NonNull User user) {
        this.id = new ParticipationId(conference.getId(), user.getId());
        this.conference = conference;
        this.user = user;
    }

    @Tolerate
    public Participation(@NonNull Conference conference, @NonNull User user, Speech speech) {
        this(conference, user);
        this.speech = speech;
        if (speech != null) {
            this.role = Role.Speaker;
        }
    }

    public enum Role {
        Guest,
        Speaker
    }
}
