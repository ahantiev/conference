package conference.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class ParticipationId implements Serializable {
    private static final long serialVersionUID = -7309358992106340685L;

    @Column(name = "conference_id")
    private Long conferenceId;
    @Column(name = "user_id")
    private Long userId;

    public ParticipationId() {
    }

    public ParticipationId(Long conferenceId, Long userId) {
        this.conferenceId = conferenceId;
        this.userId = userId;
    }
}