package conference.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Builder
@Table(name = "c_sponsor")
public class Sponsor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "conference_id")
    private Conference conference;

    @Length(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Lob
    @Column(name = "logo_url", columnDefinition="TEXT")
    private String logoUrl;

    @Lob
    @Column(name = "description", columnDefinition="TEXT")
    private String description;

    @Tolerate
    public Sponsor() {
    }
}
