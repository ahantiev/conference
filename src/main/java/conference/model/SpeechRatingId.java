package conference.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SpeechRatingId implements Serializable {
    private static final long serialVersionUID = -7309358992106340685L;
    @Column(name = "speech_id")
    private Long speechId;
    @Column(name = "user_id")
    private Long userId;

    public SpeechRatingId() {
    }

    public SpeechRatingId(Long speechId, Long userId) {
        this.speechId = speechId;
        this.userId = userId;
    }
}