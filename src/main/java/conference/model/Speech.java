package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Builder
@Table(name = "c_speech")
public class Speech {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @OneToOne
    @JoinColumn(name = "conference_id", nullable = false)
    private Conference conference;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(name = "start")
    private LocalDateTime start;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(name = "end")
    private LocalDateTime end;

    @Lob
    @Column(name = "location", columnDefinition="TEXT")
    private String location;

    @Lob
    @Column(name = "description", columnDefinition="TEXT")
    private String description;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public Speech() {
    }
}
