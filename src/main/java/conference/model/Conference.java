package conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Builder
@Table(name = "c_conference")
public class Conference {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Lob
    @Column(name = "logo_url", columnDefinition="TEXT")
    private String logoUrl;

    @Lob
    @Column(name="description", columnDefinition="TEXT")
    private String description;

    @Length(max = 100)
    @Column(name = "location", length = 100)
    private String location;

    @Column(name = "latitude", columnDefinition="Decimal(9,6)")
    private Double latitude;

    @Column(name = "longitude", columnDefinition="Decimal(9,6)")
    private Double longitude;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(name = "start")
    private LocalDateTime start;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(name = "end")
    private LocalDateTime end;

    @Lob
    @Column(name = "news_rss_url", columnDefinition="TEXT")
    private String newsRssUrl;

    @Lob
    @Column(name = "twitter_feed_url", columnDefinition="TEXT")
    private String twitterFeedUrl;

    @Lob
    @Column(name = "faq_url", columnDefinition="TEXT")
    private String faqUrl;

    @Lob
    @Column(name = "privacy_policy_url", columnDefinition="TEXT")
    private String privacyPolicyUrl;

    @Lob
    @Column(name = "license_agreement_url", columnDefinition="TEXT")
    private String licenseAgreementUrl;

    @Version
    @JsonIgnore
    private Long version;

    @Tolerate
    public Conference() {
    }
}
