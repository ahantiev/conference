package conference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AppEnvironment {
    public static final String PROFILE_LOCAL = "local";
    public static final String PROFILE_DEV = "dev";

    @Autowired
    private Environment env;

    public boolean isLocal() {
        final List<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        return activeProfiles.contains(PROFILE_LOCAL);
    }

    public boolean isEmptyProfile() {
        final List<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        return activeProfiles.isEmpty();
    }
}
