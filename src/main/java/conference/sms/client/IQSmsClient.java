package conference.sms.client;

import conference.sms.dto.IQSendRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import conference.sms.dto.IQSendResponse;

import java.util.Collections;
import java.util.List;

public class IQSmsClient implements SmsClient<IQSendResponse> {

    private static final String IQ_SMS_API_ENDPOINT = "http://json.gate.iqsms.ru/send/";
    private final String login;
    private final String password;
    private RestTemplate restTemplate;

    public IQSmsClient(String login, String password) {
        this.login = login;
        this.password = password;
        this.restTemplate = new RestTemplate();
    }

    @Override
    public IQSendResponse send(String phoneNumber, String message) {
        IQSendRequest.Message iqMessage = IQSendRequest.Message.builder()
                .phone(phoneNumber)
                .text(message)
                .build();
        return send(Collections.singletonList(iqMessage));
    }

    private IQSendResponse send(List<IQSendRequest.Message> messages) {
        IQSendRequest sendRequest = IQSendRequest.builder()
                .login(login)
                .password(password)
                .messages(messages)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> entity = new HttpEntity<>(sendRequest, headers);

        try {
            HttpEntity<IQSendResponse> response = restTemplate.exchange(
                    IQ_SMS_API_ENDPOINT,
                    HttpMethod.POST,
                    entity,
                    IQSendResponse.class);
            return response.getBody();
        } catch (Exception e) {
            return IQSendResponse.errorResponse(e);
        }
    }
}
