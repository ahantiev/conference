package conference.sms.client;

public interface SmsClient<T> {
    T send(String phoneNumber, String message);
}
