package conference.sms.client;

import conference.sms.dto.IQSendResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Collections;

@Slf4j
public class FakeIQSmsClient implements SmsClient<IQSendResponse> {
    @Override
    public IQSendResponse send(String phoneNumber, String messageText) {
        log.info("The SMS was sent to phone number: " + phoneNumber);
        IQSendResponse.Message message = IQSendResponse.Message.builder()
                .status("ok")
                .clientId("clientId")
                .smscId("smscId")
                .build();

        return IQSendResponse.builder()
                .status(IQSendResponse.STATUS_OK)
                .messages(Collections.singletonList(message))
                .build();
    }
}
