package conference.sms.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.NotBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@Builder(builderMethodName = "privateBuilder")
public class IQSendResponse {
    public static final String STATUS_OK = "ok";
    public static final String STATUS_ERROR = "error";
    @NotBlank
    private String status;
    private List<Message> messages = new ArrayList<>();
    private Throwable cause;

    @Tolerate
    public IQSendResponse() {
    }

    public static IQSendResponseBuilder builder() {
        return privateBuilder().messages(new ArrayList<>());
    }

    public boolean isOk() {
        return STATUS_OK.equals(status);
    }
    public boolean isError() {
        return STATUS_ERROR.equals(status);
    }


    public static IQSendResponse errorResponse(Throwable cause) {
        return builder().status(STATUS_ERROR).cause(cause).build();
    }

    @Setter
    @Getter
    @Builder
    public static class Message {
        private String clientId;
        private String smscId;
        private String status;

        @Tolerate
        public Message() {
        }

        public boolean isOk() {
            return STATUS_OK.equals(status);
        }
    }
}