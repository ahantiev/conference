package conference.sms.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.NotBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@Builder(builderMethodName = "privateBuilder")
public class IQSendRequest {
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    private String statusQueueName;
    private String scheduleTime;
    private List<Message> messages = new ArrayList<>();

    public static IQSendRequestBuilder builder() {
        return privateBuilder().messages(new ArrayList<>());
    }

    @Setter
    @Getter
    @Builder(builderMethodName = "privateBuilder")
    public static class Message {
        @NotBlank
        private String clientId;
        @NotBlank
        private String phone;
        @NotBlank
        private String text;
        private String sender;
        private String wapurl;

        @Tolerate
        public Message() {
            this.clientId = UUID.randomUUID().toString();
        }

        @Tolerate
        public Message(String phone, String text) {
            this();
            this.phone = phone;
            this.text = text;
        }

        public static MessageBuilder builder() {
            return privateBuilder().clientId(UUID.randomUUID().toString());
        }
    }
}