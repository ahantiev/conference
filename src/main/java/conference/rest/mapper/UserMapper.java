package conference.rest.mapper;

import conference.model.User;
import conference.rest.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserDto toDto(User user) {
        if (user == null) {
            return null;
        }

        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .phoneNumber(user.getPhoneNumber())
                .photoUrl(user.getPhotoUrl())
                .description(user.getDescription())
                .university(user.getUniversity())
                .company(user.getCompany())
                .position(user.getPosition())
                .build();
    }

}
