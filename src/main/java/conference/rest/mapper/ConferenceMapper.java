package conference.rest.mapper;

import conference.rest.dto.ConferenceDto;
import conference.rest.dto.ConferenceShortDto;
import org.springframework.stereotype.Component;
import conference.model.Conference;

@Component
public class ConferenceMapper {

    public ConferenceDto toDto(Conference c) {
        if (c == null) {
            return null;
        }

        return ConferenceDto.builder()
                .id(c.getId())
                .name(c.getName())
                .logoUrl(c.getLogoUrl())
                .description(c.getDescription())
                .location(c.getLocation())
                .latitude(c.getLatitude())
                .longitude(c.getLongitude())
                .start(c.getStart())
                .end(c.getEnd())
                .newsRssUrl(c.getNewsRssUrl())
                .twitterFeedUrl(c.getTwitterFeedUrl())
                .faqUrl(c.getFaqUrl())
                .privacyPolicyUrl(c.getPrivacyPolicyUrl())
                .licenseAgreementUrl(c.getLicenseAgreementUrl())
                .build();
    }

    public ConferenceShortDto toShortDto(Conference c) {
        if (c == null) {
            return null;
        }

        return ConferenceShortDto.builder()
                .id(c.getId())
                .name(c.getName())
                .build();
    }
}
