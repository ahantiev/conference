package conference.rest.mapper;

import conference.model.Sponsor;
import conference.rest.dto.SponsorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SponsorMapper {

    @Autowired
    private ConferenceMapper conferenceMapper;

    public SponsorDto toDto(Sponsor s) {
        if (s == null) {
            return null;
        }

        return SponsorDto.builder()
                .id(s.getId())
                .conference(conferenceMapper.toShortDto(s.getConference()))
                .name(s.getName())
                .description(s.getDescription())
                .logoUrl(s.getLogoUrl())
                .build();
    }

}
