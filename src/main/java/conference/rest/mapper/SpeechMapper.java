package conference.rest.mapper;

import conference.model.Participation;
import conference.model.Speech;
import conference.rest.dto.SpeechDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SpeechMapper {

    @Autowired
    private ConferenceMapper conferenceMapper;
    @Autowired
    private UserMapper userMapper;

    public SpeechDto toDto(Speech s) {
        if (s == null) {
            return null;
        }

        return SpeechDto.builder()
                .id(s.getId())
                .conference(conferenceMapper.toShortDto(s.getConference()))
                .start(s.getStart())
                .end(s.getEnd())
                .location(s.getLocation())
                .description(s.getDescription())
                .build();
    }

    public SpeechDto toDto(Participation p) {
        if (p == null) {
            return null;
        }

        SpeechDto speechDto = toDto(p.getSpeech());
        speechDto.setSpeaker(userMapper.toDto(p.getUser()));
        return speechDto;
    }

}
