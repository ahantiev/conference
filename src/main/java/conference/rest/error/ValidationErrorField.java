package conference.rest.error;

import lombok.Getter;

@Getter
public class ValidationErrorField {
    private String field;

    private String message;

    public ValidationErrorField(String field, String message) {
        this.field = field;
        this.message = message;
    }
}
