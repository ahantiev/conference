package conference.rest.error;

import java.util.ArrayList;
import java.util.List;

public class ValidationException extends ApiException {

    private final List<ValidationErrorField> errors = new ArrayList<>();

    public ValidationException(ValidationErrorField error) {
        super(ApiError.VALIDATION);
        errors.add(error);
    }

    public ValidationException(String field, String fieldErrorMessage) {
        super(ApiError.VALIDATION);
        errors.add(new ValidationErrorField(field, fieldErrorMessage));
    }

    public List<ValidationErrorField> getErrors() {
        return errors;
    }
}
