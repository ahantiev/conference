package conference.rest.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler()
    public ResponseEntity processApiException(Exception e) {
        ApiError apiError;
        if (e instanceof ApiException) {
            apiError = ((ApiException) e).getApiError();
        } else if (e instanceof AuthenticationException) {
            apiError = ApiError.UNAUTHORIZED;
        } else if (e instanceof AccessDeniedException) {
            apiError = ApiError.UNAUTHORIZED;
        } else {
            apiError = ApiError.INTERNAL_SERVER_ERROR;
            logger.error("Internal Server Error", e);
        }

        return new ResponseEntity<>(apiError.toMap(), apiError.getStatus());
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity processValidationException(ValidationException e) {
        ApiError apiError = e.getApiError();
        Map<String, Object> result = apiError.toMap();
        result.put("message", e.getMessage());
        result.put("fieldErrors", e.getErrors());

        return new ResponseEntity<>(result, apiError.getStatus());
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = ApiError.VALIDATION;

        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();

        Map<String, Object> result = apiError.toMap();
        result.put("fieldErrors", processFieldErrors(fieldErrors));

        return new ResponseEntity<>(result, apiError.getStatus());
    }

    private List<ValidationErrorField> processFieldErrors(List<FieldError> fieldErrors) {
        List<ValidationErrorField> errors = fieldErrors
                .stream()
                .map(fieldError -> new ValidationErrorField(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());

        return errors;
    }


}
