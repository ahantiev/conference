package conference.rest.error;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public enum ApiError {
    UNAUTHORIZED("Unauthorized", HttpStatus.UNAUTHORIZED, 1),
    FORBIDDEN("Access Denied", HttpStatus.FORBIDDEN, 2),
    INTERNAL_SERVER_ERROR("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR, 3),
    VALIDATION("Validation error", HttpStatus.BAD_REQUEST, 4),

    REGISTER_INCORRECT_PHONE_NUMBER("Incorrect phone number", HttpStatus.BAD_REQUEST, 10),
    REGISTER_IS_EXPIRED("Registration is expired", HttpStatus.BAD_REQUEST, 11),
    REGISTER_INCORRECT_PHONE_NUMBER_OR_CODE("Incorrect phone number or confirmation code", HttpStatus.BAD_REQUEST, 12),

    USER_NOT_FOUND("User not found", HttpStatus.NOT_FOUND, 20),

    CONFERENCE_NOT_FOUND("Conference not found", HttpStatus.NOT_FOUND, 30),

    SPEECH_NOT_FOUND("Speech not found", HttpStatus.NOT_FOUND, 40),

    PERSONAL_PLAN_SPEECH_NOT_FOUND("Speech not found in personal plan", HttpStatus.NOT_FOUND, 50);

    private final String message;
    private final HttpStatus status;
    private final int code;

    ApiError(String message, HttpStatus status, int code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>();
        result.put("message", this.getMessage());
        result.put("status", this.getStatus().value());
        result.put("code", this.getCode());
        return result;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }
}
