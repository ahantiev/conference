package conference.rest.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Setter
@Getter
public class RegisterConfirmDto {
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String smsCode;

    public RegisterConfirmDto() {
    }

    public RegisterConfirmDto(String phoneNumber, String smsCode) {
        this.phoneNumber = phoneNumber;
        this.smsCode = smsCode;
    }
}
