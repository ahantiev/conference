package conference.rest.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Setter
@Getter
public class RegisterConfirmResDto {
    @NotBlank
    private String token;
    @NotBlank
    private Boolean isFirstLogin;

    public RegisterConfirmResDto(String token, boolean isFirstLogin) {
        this.token = token;
        this.isFirstLogin = isFirstLogin;
    }
}
