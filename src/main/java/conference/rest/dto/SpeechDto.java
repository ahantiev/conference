package conference.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import conference.Constants;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
public class SpeechDto {
    private Long id;
    private ConferenceShortDto conference;
    private UserDto speaker;
    @JsonFormat(pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime start;
    @JsonFormat(pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime end;
    private String location;
    private String description;

    @Tolerate
    public SpeechDto() {
    }
}
