package conference.rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class SpeechRatingDto {

    @Min(1)
    @Max(5)
    @NotNull
    private Integer rating;

    public SpeechRatingDto() {
    }

    public SpeechRatingDto(Integer rating) {
        this.rating = rating;
    }
}
