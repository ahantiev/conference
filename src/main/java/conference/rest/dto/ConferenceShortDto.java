package conference.rest.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

@Setter
@Getter
@Builder
public class ConferenceShortDto {
    private Long id;
    private String name;

    @Tolerate
    public ConferenceShortDto() {
    }
}
