package conference.rest.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

@Setter
@Getter
@Builder
public class SponsorDto {
    private Long id;
    private String name;
    private String logoUrl;
    private String description;
    private ConferenceShortDto conference;

    @Tolerate
    public SponsorDto() {
    }
}
