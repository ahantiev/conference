package conference.rest.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Setter
@Getter
@Builder
public class UserDto {
    private Long id;

    @NotBlank
    @Length(max = 100)
    private String name;
    @Length(max = 20)
    private String phoneNumber;
    private String photoUrl;
    private String description;
    @Length(max = 100)
    private String university;
    @Length(max = 100)
    private String company;
    @Length(max = 100)
    private String position;

    @Tolerate
    public UserDto() {
    }
}
