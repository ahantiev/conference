package conference.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SpeechAvgRatingDto {
    private Integer rating;

    public SpeechAvgRatingDto() {
    }

    public SpeechAvgRatingDto(Integer rating) {
        this.rating = rating;
    }
}
