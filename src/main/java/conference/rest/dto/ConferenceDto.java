package conference.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import conference.Constants;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
public class ConferenceDto {
    private Long id;
    private String name;
    private String logoUrl;
    private String description;
    private String location;
    private Double latitude;
    private Double longitude;
    @JsonFormat(pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime start;
    @JsonFormat(pattern = Constants.DATE_TIME_FORMAT)
    private LocalDateTime end;
    private String newsRssUrl;
    private String twitterFeedUrl;
    private String faqUrl;
    private String privacyPolicyUrl;
    private String licenseAgreementUrl;

    @Tolerate
    public ConferenceDto() {
    }
}
