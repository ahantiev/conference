package conference.rest.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Setter
@Getter
public class RegisterDto {
    @NotBlank
    private String phoneNumber;

    public RegisterDto() {
    }

    public RegisterDto(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
