package conference.rest.controller;

import conference.rest.dto.RegisterConfirmDto;
import conference.rest.dto.RegisterConfirmResDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import conference.rest.dto.RegisterDto;
import conference.service.RegistrationService;

import javax.validation.Valid;

@RestController
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(@Valid @RequestBody final RegisterDto dto) {
       registrationService.register(dto);
    }

    @RequestMapping(value = "/register-confirm", method = RequestMethod.POST)
    public RegisterConfirmResDto registerConfirm(@Valid @RequestBody final RegisterConfirmDto dto) {
        return registrationService.registerConfirm(dto);
    }
}
