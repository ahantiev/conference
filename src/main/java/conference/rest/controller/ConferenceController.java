package conference.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import conference.rest.dto.ConferenceDto;
import conference.rest.dto.SponsorDto;
import conference.rest.dto.UserDto;
import conference.service.ConferenceService;

import java.util.List;

@RestController
public class ConferenceController {

    @Autowired
    private ConferenceService conferenceService;

    @RequestMapping(value = "/conferences", method = RequestMethod.GET)
    public List<ConferenceDto> getConferences() {
        return conferenceService.findAll();
    }

    @RequestMapping(value = "/conferences/{conferenceId}", method = RequestMethod.GET)
    public ConferenceDto getConference(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.findById(conferenceId);
    }

    @RequestMapping(value = "/conferences/{conferenceId}/speakers", method = RequestMethod.GET)
    public List<UserDto> getSpeakers(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.findSpeakers(conferenceId);
    }

    @RequestMapping(value = "/conferences/{conferenceId}/guests", method = RequestMethod.GET)
    public List<UserDto> getGuests(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.findGuests(conferenceId);
    }

    @RequestMapping(value = "/conferences/{conferenceId}/sponsors", method = RequestMethod.GET)
    public List<SponsorDto> getSponsors(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.getSponsors(conferenceId);
    }
}
