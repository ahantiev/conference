package conference.rest.controller;

import conference.rest.dto.SpeechAvgRatingDto;
import conference.rest.dto.SpeechDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import conference.AppContext;
import conference.model.User;
import conference.rest.dto.SpeechRatingDto;
import conference.service.SpeechService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class SpeechController {

    @Autowired
    private SpeechService speechService;

    @RequestMapping(value = "/speeches", method = RequestMethod.GET)
    public List<SpeechDto> getSpeeches() {
        return speechService.findAll();
    }

    @RequestMapping(value = "/speeches/{speechId}/avg-rating", method = RequestMethod.GET)
    public SpeechAvgRatingDto getSpeechAvgRating(@PathVariable("speechId") Long speechId) {
        return speechService.findAvgRating(speechId);
    }

    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value = "/speeches/{speechId}/rating", method = RequestMethod.POST)
    public void setSpeechRating(@PathVariable("speechId") Long speechId, @Valid @RequestBody final SpeechRatingDto dto) {
        User user = AppContext.getAuthenticatedUser();
        speechService.setSpeechRating(speechId, user.getId(), dto);
    }
}
