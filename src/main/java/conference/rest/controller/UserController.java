package conference.rest.controller;

import conference.model.User;
import conference.rest.dto.UserDto;
import conference.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import conference.AppContext;

import javax.validation.Valid;


@RestController
@PreAuthorize("hasAuthority('USER')")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/users/me", method = RequestMethod.PUT)
    public UserDto updateUser(@Valid @RequestBody final UserDto dto) {
        User user = AppContext.getAuthenticatedUser();
        return userService.update(user.getId(), dto);
    }

    @RequestMapping(value = "/users/me", method = RequestMethod.GET)
    public UserDto getCurrentUser() {
        User user = AppContext.getAuthenticatedUser();
        return userService.findById(user.getId());
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public UserDto getUserById(@PathVariable("userId") Long userId) {
        return userService.findById(userId);
    }


}
