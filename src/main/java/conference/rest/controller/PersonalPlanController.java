package conference.rest.controller;

import conference.model.User;
import conference.rest.dto.SpeechDto;
import conference.service.PersonalPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import conference.AppContext;

import java.util.List;


@RestController
@PreAuthorize("hasAuthority('USER')")
public class PersonalPlanController {

    @Autowired
    private PersonalPlanService personalPlanService;

    @RequestMapping(value = "/personal-plan", method = RequestMethod.GET)
    public List<SpeechDto> getPersonalPlan() {
        User user = AppContext.getAuthenticatedUser();
        return personalPlanService.getPersonalPlan(user.getId());
    }

    @RequestMapping(value = "/personal-plan/{speechId}", method = RequestMethod.POST)
    public void addSpeechToPersonalPlan(@PathVariable("speechId") Long speechId) {
        User user = AppContext.getAuthenticatedUser();
        personalPlanService.addSpeechToPersonalPlan(user.getId(), speechId);
    }

    @RequestMapping(value = "/personal-plan/{speechId}", method = RequestMethod.DELETE)
    public void removeSpeechFromPersonalPlan(@PathVariable("speechId") Long speechId) {
        User user = AppContext.getAuthenticatedUser();
        personalPlanService.removeSpeechFromPersonalPlan(user.getId(), speechId);
    }

    @RequestMapping(value = "/personal-plan/clean", method = RequestMethod.DELETE)
    public void clearPersonalPlan() {
        User user = AppContext.getAuthenticatedUser();
        personalPlanService.clearPersonalPlan(user.getId());
    }




}
