package conference.security;

import conference.model.User;
import conference.repository.UserRepository;
import conference.rest.error.ApiError;
import conference.rest.error.ApiException;
import conference.security.token.UserTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class XAuthTokenAuthenticator {

    @Autowired
    private UserTokenProvider userTokenProvider;

    @Autowired
    private UserRepository userRepository;

    public User authenticate(HttpServletRequest req) {

        final String authToken = req.getHeader(UserTokenProvider.USER_AUTH_TOKEN_HEADER);
        if (authToken == null) {
            return null;
        }

        if (!userTokenProvider.isValid(authToken)) {
            throw new ApiException(ApiError.UNAUTHORIZED);
        }

        final Integer userId = userTokenProvider.getUserId(authToken);

        return userRepository.findOneById(userId.longValue())
                .orElseThrow(() -> new ApiException(ApiError.UNAUTHORIZED));
    }

}
