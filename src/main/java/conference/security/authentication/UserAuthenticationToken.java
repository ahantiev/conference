package conference.security.authentication;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;


public class UserAuthenticationToken extends AbstractAuthenticationToken {

    private final UserPrincipal principal;

    public UserAuthenticationToken(UserPrincipal userPrincipal) {
        super(Collections.singletonList(userPrincipal));
        this.principal = userPrincipal;
        super.setAuthenticated(true); // must use super, as we override
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public UserPrincipal getPrincipal() {
        return principal;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        throw new IllegalStateException("This should not be called");
    }
}
