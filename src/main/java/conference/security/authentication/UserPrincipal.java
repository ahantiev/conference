package conference.security.authentication;

import conference.model.User;
import org.springframework.security.core.GrantedAuthority;

public class UserPrincipal implements GrantedAuthority {

    public static final String USER_AUTHORITY = "USER";
    private final User user;

    public UserPrincipal(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String getAuthority() {
        return USER_AUTHORITY;
    }
}
