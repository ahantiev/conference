package conference.security.token;

import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

public class UserTokenProvider {

    public static final String USER_AUTH_TOKEN_HEADER = "x-auth-token";

    private final JwtHelper jwtHelper;

    public UserTokenProvider(@NonNull String secretKey, @NonNull int tokenValidityInMinutes) {
        this.jwtHelper = new JwtHelper(secretKey, tokenValidityInMinutes);
    }

    public String create(Long userId) {
        return jwtHelper.createToken(userId);
    }

    public boolean isValid(String token) {
        return jwtHelper.isValid(token);
    }

    public Integer getUserId(String token) {
        return jwtHelper.getClaim(token, JwtHelper.USER_ID_CLAIM).asInt();
    }

}
