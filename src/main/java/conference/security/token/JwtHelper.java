package conference.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

@Slf4j
public class JwtHelper {

    private static final long MILLIS_IN_SECOND = 1000L;
    public static final String USER_ID_CLAIM = "userId";

    private final String secretKey;
    private final int tokenValidityInMinutes;
    private final JWTVerifier verifier;

    public JwtHelper(String secretKey, int tokenValidityInMinutes) {
        this.secretKey = secretKey;
        this.tokenValidityInMinutes = tokenValidityInMinutes;
        try {
            this.verifier = JWT.require(Algorithm.HMAC256(secretKey))
                    .withIssuer("auth0")
                    .build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Error creating verifier", e);
        }
    }

    public boolean isValid(String token) {
        try {
            verifier.verify(token);
        } catch (Exception e) {
            log.debug("JWT Token parse error", e);
            return false;
        }
        return true;
    }

    public String createToken(Long userId) {
//        final LocalDateTime expDate = TimeUtils.now().plusMinutes(tokenValidityInMinutes);
//        final long expires = expDate.toInstant(ZoneOffset.UTC).toEpochMilli() / MILLIS_IN_SECOND;

        String token;
        try {
            token = JWT.create()
                    .withIssuer("auth0")
                    .withClaim(USER_ID_CLAIM, userId.intValue())
//                    .withExpiresAt(new Date(expires))
                    .sign(Algorithm.HMAC256(secretKey));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Error creating token", e);
        }
        return token;
    }

    public Claim getClaim(String token, String claimKey) {
        return verifier
                .verify(token)
                .getClaim(claimKey);
    }

}
