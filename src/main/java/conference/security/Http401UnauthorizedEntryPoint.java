package conference.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import conference.rest.error.ApiError;
import conference.utils.RequestUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Returns a 401 error code (Unauthorized) to the client.
 */
@Component
@Slf4j
public class Http401UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    /**
     * Always returns a 401 error code to the client.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException,
            ServletException {

        if (exception != null) {
            log.warn("Unexpected exception", exception);
        } else {
            log.debug("Pre-authenticated entry point called. Rejecting access");
        }
        RequestUtils.writeApiErrorToResponse(ApiError.UNAUTHORIZED, response);
    }
}
