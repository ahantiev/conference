package conference.security;

import conference.model.User;
import conference.rest.error.ApiException;
import conference.security.authentication.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.GenericFilterBean;
import conference.AppContext;
import conference.rest.error.ApiError;
import conference.utils.RequestUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class XAuthTokenFilter extends GenericFilterBean {

    private final XAuthTokenAuthenticator authTokenAuthenticator;

    public XAuthTokenFilter(XAuthTokenAuthenticator authTokenAuthenticator) {
        this.authTokenAuthenticator = authTokenAuthenticator;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        final HttpServletResponse res = (HttpServletResponse) response;
        final HttpServletRequest req = (HttpServletRequest) request;

        if ("OPTIONS".equalsIgnoreCase(req.getMethod())) {
            filterChain.doFilter(request, res);
            return;
        }

        try {
            User user = authTokenAuthenticator.authenticate(req);
            if (user != null) {
                AppContext.setPrincipal(new UserPrincipal(user));
            }

            filterChain.doFilter(request, res);
        } catch (ApiException apiEx) {
            RequestUtils.writeApiErrorToResponse(apiEx.getApiError(), response);
        } catch (Exception ex) {
            logger.warn("Unexpected exception", ex);
            RequestUtils.writeApiErrorToResponse(ApiError.UNAUTHORIZED, response);
        }
    }

}
